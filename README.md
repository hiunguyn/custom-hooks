<p align="center">
  <a href="https://reactjs.org/">
    <img src="https://hiunguyn.github.io/static-file/react-native-guide/react-native.png" height="128">
    <h1 align="center">React Js</h1>
  </a>
</p>

### Start development

```bash
$ cp .env.example .env

$ npm i

$ npm run start
```

### Build production

```bash
$ cp .env.example .env

$ npm i

$ npm run build:prod
```

### Lint

```bash
$ npm run lint

$ npm run lint:fix
```

## Check list

- [ ] Ckecklist 1
- [ ] Ckecklist 2
- [ ] Ckecklist 3
- [ ] Ckecklist 4
- [ ] Ckecklist 5
- [ ] Ckecklist 6
