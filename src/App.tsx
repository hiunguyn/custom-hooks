import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { QueryClient, QueryClientProvider } from 'react-query'

import '@/utils/i18next'
import '@/styles/global.scss'

import { AppContextProvider } from '@/contexts/app.context'
import PrivateRoute from '@/common/PrivateRoute'
import Login from '@/pages/auth/Login'
import Home from '@/pages/home/Home'
import ErrorPage from '@/pages/error/ErrorPage'

import Debounce from '@/pages/custom-hook/Debounce'
import Timeout from '@/pages/custom-hook/Timeout'
import Input from '@/pages/custom-hook/Input'
import Storage from '@/pages/custom-hook/Storage'

const queryClient = new QueryClient()

const AllRoute = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/login" component={Login} />
      <PrivateRoute exact path="/" component={Home} />

      <Route path="/custom-hook/use-input" component={Input} />
      <Route path="/custom-hook/use-timeout" component={Timeout} />
      <Route path="/custom-hook/use-debounce" component={Debounce} />
      <Route path="/custom-hook/use-storage" component={Storage} />

      <Route path="*" component={ErrorPage} />
    </Switch>
  </BrowserRouter>
)

const App = () => (
  <QueryClientProvider client={queryClient}>
    <AppContextProvider>
      <AllRoute />
    </AppContextProvider>
  </QueryClientProvider>
)

export default App
