import { AppActionType, AppStateType } from '@/contexts/app.type'
import { ActionType } from '@/contexts/app.action'

export const initialState: AppStateType = {
  name: 'Hieu Nguyen',
}

export const AppReducer = (state: AppStateType, action: AppActionType): AppStateType => {
  switch (action.type) {
    case ActionType.SOME_ACTION:
      return { ...state, ...action.payload }

    default:
      return state
  }
}
