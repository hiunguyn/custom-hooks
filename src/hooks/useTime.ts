import React from 'react'

import { formatDateTime } from '@/utils/date'

export const useTime = () => {
  const [time, setTime] = React.useState(formatDateTime(new Date()))

  React.useEffect(() => {
    const timeID = setInterval(() => setTime(formatDateTime(new Date())), 1000)
    return () => clearInterval(timeID)
  }, [])

  return time
}
