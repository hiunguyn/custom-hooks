import React, { useState } from 'react'

import { useDebounce } from '@/hooks/useDebounce'
import { useInput } from '@/hooks/useInput'

const Debounce = () => {
  const [keyword, setKeyword] = useState('')
  const [value, onChange] = useInput()

  useDebounce(() => setKeyword(value), 500, [value])

  return (
    <div>
      <h3>Keyword: {keyword}</h3>
      <input type="text" value={value} onChange={onChange} />
    </div>
  )
}

export default Debounce
