import React from 'react'

import { useInput } from '@/hooks/useInput'
import { useTime } from '@/hooks/useTime'

const Input = () => {
  const [value, onChange] = useInput()
  const time = useTime()

  return (
    <div>
      <h3>Current time: {time}</h3>
      <h3>Value: {value}</h3>
      <input type="text" value={value} onChange={onChange} />
    </div>
  )
}

export default Input
