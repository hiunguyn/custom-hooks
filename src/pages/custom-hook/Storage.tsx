import React from 'react'

import { useLocalStorage, useSessionStorage } from '@/hooks/useStorage'

const Storage = () => {
  const [name, setName, removeName] = useSessionStorage('name', 'Hieu Nguyen')
  const [age, setAge, removeAge] = useLocalStorage('age', 22)

  return (
    <div>
      <div>
        {name} - {age}
      </div>
      <button type="button" onClick={() => setName('John')}>Set Name</button>
      <button type="button" onClick={() => setAge(40)}>Set Age</button>
      <button type="button" onClick={removeName}>Remove Name</button>
      <button type="button" onClick={removeAge}>Remove Age</button>
    </div>
  )
}

export default Storage
