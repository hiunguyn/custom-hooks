import React, { useState } from 'react'

import { useTimeout } from '@/hooks/useTimeout'

const Timeout = () => {
  const [count, setCount] = useState(10)
  const { clear, reset } = useTimeout(() => setCount(0), 1000)

  return (
    <div>
      <div>Count: {count}</div>
      <button type="button" onClick={() => setCount((c) => c + 1)}>Increment</button>
      <button type="button" onClick={clear}>Clear Timeout</button>
      <button type="button" onClick={reset}>Reset Timeout</button>
    </div>
  )
}

export default Timeout
